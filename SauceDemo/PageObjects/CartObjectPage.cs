﻿using OpenQA.Selenium;
using SauceDemo.Base;
using SeleniumExtras.PageObjects;

namespace SauceDemo.PageObjects
{
    internal class CartObjectPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "continue-shopping")]
        private readonly IWebElement ContinueShoppingButton;

        [FindsBy(How = How.Id, Using = "checkout")]
        private readonly IWebElement CheckoutButton;

        private By CartItemElement = By.ClassName("cart_item");

        public CartObjectPage()
        {
            PageFactory.InitElements(_webDriver, this);
        }

        public int GetCartItemsCount()
        {
            var result = _webDriver.FindElements(CartItemElement);
            return result.Count;
        }

        public InventoryPageObject ContinueShoppingButtonClick()
        {
            ContinueShoppingButton.Click();
            return new InventoryPageObject();
        }

        public CheckoutPageObject CheckoutButtonClick()
        {
            CheckoutButton.Click();
            return new CheckoutPageObject();
        }
    }
}
