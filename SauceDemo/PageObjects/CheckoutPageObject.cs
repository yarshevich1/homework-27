﻿using OpenQA.Selenium;
using SauceDemo.Base;
using SeleniumExtras.PageObjects;

namespace SauceDemo.PageObjects
{
    internal class CheckoutPageObject : BasePage
    {
        [FindsBy(How = How.Id, Using = "cancel")]
        private readonly IWebElement CancelButton;

        [FindsBy(How = How.Id, Using = "continue")]
        private readonly IWebElement continueButton;

        [FindsBy(How = How.Id, Using = "first-name")]
        private readonly IWebElement FirstNameInput;

        [FindsBy(How = How.Id, Using = "last-name")]
        private readonly IWebElement LastNameInput;

        [FindsBy(How = How.Id, Using = "postal-code")]
        private readonly IWebElement PostalCodeInput;

        public CheckoutPageObject()
        {
            PageFactory.InitElements(_webDriver, this);
        }

        public CartObjectPage CancelButtonClick()
        {
            CancelButton.Click();
            return new CartObjectPage();
        }
    }
}
