﻿using ExtentReportLib.Reports;
using OpenQA.Selenium;
using SauceDemo.Base;
using SeleniumExtras.PageObjects;

namespace SauceDemo.PageObjects
{
    internal class LoginPageObject : BasePage
    {
        [FindsBy(How = How.Id, Using = "user-name")] 
        private readonly IWebElement UserNameField;

        [FindsBy(How = How.Id, Using = "password")]
        private readonly IWebElement PasswordField;

        [FindsBy(How = How.Id, Using = "login-button")]
        private readonly IWebElement LoginButton;

        public LoginPageObject()
        {
          //  ExtentReporting.LogInfo($"Initializate page login");
            PageFactory.InitElements(_webDriver, this);
        }

        public LoginPageObject EnterUsername(string username)
        {
          //  ExtentReporting.LogInfo($"Enter user name: {username}");
            UserNameField.SendKeys(username);
            return this;
        }

        public LoginPageObject EnterPassword(string password)
        {
          //  ExtentReporting.LogInfo($"Enter password: {password}");
            PasswordField.SendKeys(password);
            return this;
        }

        public void ClickLoginButton()
        {
         //   ExtentReporting.LogInfo($"Click to login button");
            LoginButton.Click();
        }

        public InventoryPageObject Login(string username, string password)
        {
            EnterUsername(username)
                .EnterPassword(password)
                .ClickLoginButton();

            return new InventoryPageObject();
        }
    }
}
