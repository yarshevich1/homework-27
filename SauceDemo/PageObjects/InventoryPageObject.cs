﻿using ExtentReportLib.Reports;
using OpenQA.Selenium;
using SauceDemo.Base;
using SeleniumExtras.PageObjects;
using SeleniumExtras.WaitHelpers;

namespace SauceDemo.PageObjects
{
    internal class InventoryPageObject : BasePage
    {
        private readonly By logoAppElement = By.ClassName("app_logo");

        private readonly By ErrorTextElement = By.XPath("//*[@data-test='error']");

        [FindsBy(How = How.Id, Using = "item_4_title_link")]
        private readonly IWebElement SauceLabsBackpackElement;

        [FindsBy(How = How.Id, Using = "item_0_title_link")]
        private readonly IWebElement SauceLabsBikeLightElement;

        [FindsBy(How = How.Id, Using = "item_1_title_link")]
        private readonly IWebElement SauceLabsBoltTShirtElement;

        [FindsBy(How = How.Id, Using = "item_5_title_link")]
        private readonly IWebElement SauceLabsFleeceJacketElement;

        [FindsBy(How = How.Id, Using = "item_2_title_link")]
        private readonly IWebElement SauceLabsOnesieElement;

        [FindsBy(How = How.Id, Using = "item_3_title_link")]
        private readonly IWebElement TestAllTheThingsTShirtRedElement;

        [FindsBy(How = How.Id, Using = "add-to-cart-sauce-labs-backpack")]
        private readonly IWebElement AddToCartSauceLabsBackpackButton;

        [FindsBy(How = How.Id, Using = "add-to-cart-sauce-labs-bike-light")]
        private readonly IWebElement AddToCartSauceLabsBikeLightButton;

        [FindsBy(How = How.Id, Using = "add-to-cart-sauce-labs-bolt-t-shirt")]
        private readonly IWebElement AddToCartSauceLabsBoltTShirtButton;

        [FindsBy(How = How.Id, Using = "add-to-cart-sauce-labs-fleece-jacket")]
        private readonly IWebElement AddToCartSauceLabsFleeceJacketButton;

        [FindsBy(How = How.Id, Using = "add-to-cart-test.allthethings()-t-shirt-(red)")]
        private readonly IWebElement AddToCartTestAllTheThingsTShirtRedButton;

        [FindsBy(How = How.Id, Using = "add-to-cart-sauce-labs-onesie")]
        private readonly IWebElement AddToCartSauceLabsOnesieButton;

        [FindsBy(How= How.Id, Using = "shopping_cart_container")]
        private readonly IWebElement GoToCartButton;


        public InventoryPageObject()
        {
           // ExtentReporting.LogInfo($"Initializate page inventory");
            PageFactory.InitElements(_webDriver, this);
        }

        public bool IsLogoVisible()
        {
            try
            {
                _wait.Until(ExpectedConditions.ElementIsVisible(logoAppElement));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsLocked()
        {
            try
            {
                _wait.Until(ExpectedConditions.ElementIsVisible(ErrorTextElement));
                string text = _webDriver.FindElement(ErrorTextElement).Text;
                return text == "Epic sadface: Sorry, this user has been locked out.";
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public InventoryPageObject AddOrRemoveToCartSauceLabsBackpack()
        {
          //  ExtentReporting.LogInfo($"Click to Add To Cart Sauce Lab sBackpack Button");
            AddToCartSauceLabsBackpackButton.Click();
            return this;
        }

        public InventoryPageObject AddOrRemoveToCartSauceLabsBikeLight()
        {
           // ExtentReporting.LogInfo($"Click to Add To Cart Sauce Labs Bike Light Button");
            AddToCartSauceLabsBikeLightButton.Click();
            return this;
        }

        public InventoryPageObject AddOrRemoveToCartSauceLabsBoltTShirt()
        {
           // ExtentReporting.LogInfo($"Click to Add To Cart Sauce Labs Bolt T-Shirt Button");
            AddToCartSauceLabsBoltTShirtButton.Click();
            return this;
        }

        public InventoryPageObject AddOrRemoveToCartSauceLabsFleeceJacket()
        {
           // ExtentReporting.LogInfo($"Click to Add To Cart Sauce Labs Fleece Jacket Button");
            AddToCartSauceLabsFleeceJacketButton.Click();
            return this;
        }

        public InventoryPageObject AddOrRemoveToCartTestAllTheThingsTShirtRed()
        {
           // ExtentReporting.LogInfo($"Click to Add To Cart Test All The Things T-Shirt Red Button");
            AddToCartTestAllTheThingsTShirtRedButton.Click();
            return this;
        }

        public InventoryPageObject AddOrRemoveToCartSauceLabsOnesie()
        {
           // ExtentReporting.LogInfo($"Click to Add To Cart Sauce Labs Onesie Button");
            AddToCartSauceLabsOnesieButton.Click();
            return this;
        }

        public InventoryPageObject AddToCardAllItems()
        {
            AddOrRemoveToCartSauceLabsBackpack()
                .AddOrRemoveToCartSauceLabsBikeLight()
                .AddOrRemoveToCartSauceLabsFleeceJacket()
                .AddOrRemoveToCartSauceLabsBoltTShirt()
                .AddOrRemoveToCartTestAllTheThingsTShirtRed()
                .AddOrRemoveToCartSauceLabsOnesie();

            return this;
        }

        public InventoryPageObject AddToCardThreeItems()
        {
            AddOrRemoveToCartSauceLabsBackpack()
                .AddOrRemoveToCartSauceLabsFleeceJacket()
                .AddOrRemoveToCartSauceLabsBoltTShirt();

            return this;
        }

        public CartObjectPage GoToCart()
        {
           // ExtentReporting.LogInfo($"Click to Go to cart button");
            GoToCartButton.Click();
            return new CartObjectPage();
        }
    }
}
