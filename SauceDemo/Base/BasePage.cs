﻿using Core.SeleniumDriver;
using ExtentReportLib.Reports;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.Events;
using OpenQA.Selenium.Support.UI;

namespace SauceDemo.Base
{
    internal class BasePage
    {
        protected IWebDriver _webDriver;
        protected WebDriverWait _wait;

        public BasePage()
        {
            // _webDriver = Browser.Instance.WebDriver;
            // _wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
            var handlerDriver = new EventFiringWebDriver(Browser.Instance.WebDriver);
            handlerDriver.ElementClicked += HandlerDriver_ElementClicked;
            handlerDriver.ElementValueChanged += HandlerDriver_ElementValueChanged;
            handlerDriver.ExceptionThrown += HandlerDriver_ExceptionThrown;
            handlerDriver.FindingElement += HandlerDriver_FindingElement;
            handlerDriver.FindElementCompleted += HandlerDriver_FindElementCompleted;
            handlerDriver.ScriptExecuted += HandlerDriver_ScriptExecuted;
            _webDriver = handlerDriver;
            _wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
        }

        private void HandlerDriver_ScriptExecuted(object? sender, WebDriverScriptEventArgs e)
        {
           // ExtentReporting.LogInfo($"Script executed: {e.Script}");
            Console.WriteLine($"Script executed: {e.Script}");
        }

        private void HandlerDriver_FindElementCompleted(object? sender, FindElementEventArgs e)
        {
          //  ExtentReporting.LogInfo($"Find element completed: {e.Element}");
            Console.WriteLine($"Find element completed: {e.Element}");
        }

        private void HandlerDriver_FindingElement(object? sender, FindElementEventArgs e)
        {
           // ExtentReporting.LogInfo($"Finding element: {e.Element}");
            Console.WriteLine($"Finding element: {e.Element}");
        }

        private void HandlerDriver_ExceptionThrown(object? sender, WebDriverExceptionEventArgs e)
        {
          //  ExtentReporting.LogInfo($"Exception thrown: {e.ThrownException}");
            Console.WriteLine($"Exception thrown: {e.ThrownException}");
        }

        private void HandlerDriver_ElementValueChanged(object? sender, WebElementValueEventArgs e)
        {
          // ExtentReporting.LogInfo($"Element {e.Value} value changed");
            Console.WriteLine($"Element {e.Value} value changed");
        }

        private void HandlerDriver_ElementClicked(object? sender, WebElementEventArgs e)
        {
           // ExtentReporting.LogInfo($"{e.Element} clicked");
            Console.WriteLine($"{e.Element} clicked");
        }
    }
}
