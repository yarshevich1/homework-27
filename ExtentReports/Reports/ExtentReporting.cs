﻿//using AventStack.ExtentReports;
//using System.Reflection;
//using AventStack.ExtentReports.Reporter;
//using ExtentReports = AventStack.ExtentReports.ExtentReports;

namespace ExtentReportLib.Reports
{
    public class ExtentReporting
    {
        //private static ExtentReports extentReports;
        //private static ExtentTest extentTest;

        ///// <summary>
        ///// Создание отчета и прикрепление ExtentHtmlReporter
        ///// </summary>
        ///// <returns></returns>
        //private static ExtentReports StartReporting()
        //{
        //    var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"..\..\..\..\results\";

        //    if (extentReports == null)
        //    {
        //        Directory.CreateDirectory(path);

        //        extentReports = new ExtentReports();
        //        var htmlReporter = new ExtentHtmlReporter(path);

        //        extentReports.AttachReporter(htmlReporter);
        //    }

        //    return extentReports;
        //}

        ///// <summary>
        ///// Добавление нового теста в отчет
        ///// </summary>
        ///// <param name="testName"></param>
        //public static void CreateTest(string testName)
        //{
        //    extentTest = StartReporting().CreateTest(testName);
        //}

        ///// <summary>
        ///// Обновление онформации в отчете
        ///// </summary>
        //public static void EndReporting()
        //{
        //    StartReporting().Flush();
        //}

        ///// <summary>
        ///// Добавление информационного сообщения
        ///// </summary>
        ///// <param name="info"></param>
        //public static void LogInfo(string info)
        //{
        //    extentTest.Info(info);
        //}

        ///// <summary>
        ///// Добавление успешного прохождение теста
        ///// </summary>
        ///// <param name="info"></param>
        //public static void LogPass(string info)
        //{
        //    extentTest.Pass(info);
        //}

        ///// <summary>
        ///// Добавление ошибки при прохождении
        ///// </summary>
        ///// <param name="info"></param>
        //public static void LogFail(string info)
        //{
        //    extentTest.Fail(info);
        //}

        ///// <summary>
        ///// Добавление скриншота
        ///// </summary>
        ///// <param name="info"></param>
        ///// <param name="image"></param>
        //public static void LogScreenshot(string info, string image)
        //{
        //    extentTest.Info(info, MediaEntityBuilder.CreateScreenCaptureFromBase64String(image).Build());
        //}
    }
}
