﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal class ChromeDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            var chromeOptions = new ChromeOptions();

            if (Configuration.Browser.HeadLess)
                chromeOptions.AddArgument("--headless");

            chromeOptions.AddArgument("--no-sandbox");

            // var chromeDriver = new ChromeDriver(chromeOptions);
            var remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL");
            return new RemoteWebDriver(new Uri(remoteUrl), chromeOptions);

           // return new RemoteWebDriver(chromeOptions);
        }
    }
}
