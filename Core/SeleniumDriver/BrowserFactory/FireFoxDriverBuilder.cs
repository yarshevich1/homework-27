﻿using Core.Config;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace Core.SeleniumDriver.BrowserFactory
{
    internal class FireFoxDriverBuilder : DriverBuilder
    {
        public override IWebDriver GetDriver()
        {
            var firefoxOptions = new FirefoxOptions();

            if (Configuration.Browser.HeadLess)
                firefoxOptions.AddArgument("--headless");

            firefoxOptions.AddArgument("--no-sandbox");

            var remoteUrl = Environment.GetEnvironmentVariable("SELENIUM_REMOTE_URL");
            return new RemoteWebDriver(new Uri(remoteUrl), firefoxOptions);

            //var firefoxDriver = new FirefoxDriver(firefoxOptions);

            //return firefoxDriver;
        }
    }
}
